# Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/container-images/cudagl/badges/ubuntu16.04/build.svg)](https://gitlab.com/nvidia/container-images/cudagl/commits/ubuntu16.04)

## CUDA 10.1 update 2 + OpenGL (glvnd 1.1)

- `10.1-base-ubuntu16.04` [(*10.1/base/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.1/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
- `10.1-runtime-ubuntu16.04` [(*10.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.1/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/runtime/Dockerfile)
- `10.1-devel-ubuntu16.04` [(*10.1/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.1/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)

## CUDA 10.0 + OpenGL (glvnd 1.1)

- `10.0-base-ubuntu16.04` [(*10.0/base/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.0/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
- `10.0-runtime-ubuntu16.04` [(*10.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.0/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/runtime/Dockerfile)
- `10.0-devel-ubuntu16.04` [(*10.0/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/10.0/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)

## CUDA 9.2 + OpenGL (glvnd 1.1)

- `9.2-base`, `9.2-base-ubuntu16.04` [(*9.2/base/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.2/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
- `9.2-runtime`, `9.2-runtime-ubuntu16.04` [(*9.2/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.2/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/runtime/Dockerfile)
- `9.2-devel`, `9.2-devel-ubuntu16.04` [(*9.2/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.2/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)

## CUDA 9.1 + OpenGL (glvnd 1.1)

- `9.1-base`, `9.1-base-ubuntu16.04` [(*9.1/base/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.1/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
- `9.1-runtime`, `9.1-runtime-ubuntu16.04` [(*9.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.1/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/runtime/Dockerfile)
- `9.1-devel`, `9.1-devel-ubuntu16.04` [(*9.1/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.1/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)

## CUDA 9.0 + OpenGL (glvnd 1.1)

- `9.0-base`, `9.0-base-ubuntu16.04` [(*9.0/base/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
- `9.0-runtime`, `9.0-runtime-ubuntu16.04` [(*9.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/runtime/Dockerfile)
- `9.0-devel`, `9.0-devel-ubuntu16.04` [(*9.0/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu16.04/glvnd/devel/Dockerfile)
